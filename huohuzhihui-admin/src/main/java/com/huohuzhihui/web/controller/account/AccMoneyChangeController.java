package com.huohuzhihui.web.controller.account;

import com.huohuzhihui.account.domain.AccMoneyChange;
import com.huohuzhihui.account.service.IAccMoneyChangeService;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 帐户增减款Controller
 * 
 * @author huohuzhihui
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/account/moneyChange")
public class AccMoneyChangeController extends BaseController
{
    @Autowired
    private IAccMoneyChangeService accMoneyChangeService;

    /**
     * 查询帐户增减款列表
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:list')")
    @GetMapping("/list")
    public TableDataInfo list(AccMoneyChange accMoneyChange)
    {
        startPage();
        List<AccMoneyChange> list = accMoneyChangeService.selectAccMoneyChangeList(accMoneyChange);
        return getDataTable(list);
    }

    /**
     * 导出帐户增减款列表
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:export')")
    @Log(title = "帐户增减款", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AccMoneyChange accMoneyChange)
    {
        List<AccMoneyChange> list = accMoneyChangeService.selectAccMoneyChangeList(accMoneyChange);
        ExcelUtil<AccMoneyChange> util = new ExcelUtil<AccMoneyChange>(AccMoneyChange.class);
        return util.exportExcel(list, "moneyadd");
    }

    /**
     * 获取帐户增减款详细信息
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(accMoneyChangeService.selectAccMoneyChangeById(id));
    }

    /**
     * 新增帐户增减款
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:add')")
    @Log(title = "帐户增减款", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AccMoneyChange accMoneyChange)
    {
        accMoneyChange.setCreateBy(SecurityUtils.getUsername());
        return toAjax(accMoneyChangeService.insertAccMoneyChange(accMoneyChange));
    }

    /**
     * 修改帐户增减款
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:edit')")
    @Log(title = "帐户增减款", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AccMoneyChange accMoneyChange)
    {
        return toAjax(accMoneyChangeService.updateAccMoneyChange(accMoneyChange));
    }

    /**
     * 删除帐户增减款
     */
    @PreAuthorize("@ss.hasPermi('account:moneyadd:remove')")
    @Log(title = "帐户增减款", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accMoneyChangeService.deleteAccMoneyChangeByIds(ids));
    }
}
