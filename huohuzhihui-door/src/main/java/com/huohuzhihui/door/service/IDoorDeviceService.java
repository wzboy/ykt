package com.huohuzhihui.door.service;

import java.util.List;
import com.huohuzhihui.door.domain.DoorDevice;

/**
 * 门禁设备Service接口
 * 
 * @author huohuzhihui
 * @date 2021-08-09
 */
public interface IDoorDeviceService 
{
    /**
     * 查询门禁设备
     * 
     * @param id 门禁设备ID
     * @return 门禁设备
     */
    public DoorDevice selectDoorDeviceById(Long id);

    /**
     * 查询门禁设备列表
     * 
     * @param doorDevice 门禁设备
     * @return 门禁设备集合
     */
    public List<DoorDevice> selectDoorDeviceList(DoorDevice doorDevice);

    /**
     * 新增门禁设备
     * 
     * @param doorDevice 门禁设备
     * @return 结果
     */
    public int insertDoorDevice(DoorDevice doorDevice);

    /**
     * 修改门禁设备
     * 
     * @param doorDevice 门禁设备
     * @return 结果
     */
    public int updateDoorDevice(DoorDevice doorDevice);

    /**
     * 批量删除门禁设备
     * 
     * @param ids 需要删除的门禁设备ID
     * @return 结果
     */
    public int deleteDoorDeviceByIds(Long[] ids);

    /**
     * 删除门禁设备信息
     * 
     * @param id 门禁设备ID
     * @return 结果
     */
    public int deleteDoorDeviceById(Long id);


}
