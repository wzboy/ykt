package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaMeeting;

import java.util.List;

/**
 * 会议申请Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaMeetingService 
{
    /**
     * 查询会议申请
     * 
     * @param id 会议申请ID
     * @return 会议申请
     */
    public OaMeeting selectOaMeetingById(Long id);

    /**
     * 查询会议申请列表
     * 
     * @param oaMeeting 会议申请
     * @return 会议申请集合
     */
    public List<OaMeeting> selectOaMeetingList(OaMeeting oaMeeting);

    /**
     * 新增会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    public int insertOaMeeting(OaMeeting oaMeeting);

    /**
     * 修改会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    public int updateOaMeeting(OaMeeting oaMeeting);

    /**
     * 批量删除会议申请
     * 
     * @param ids 需要删除的会议申请ID
     * @return 结果
     */
    public int deleteOaMeetingByIds(Long[] ids);

    /**
     * 删除会议申请信息
     * 
     * @param id 会议申请ID
     * @return 结果
     */
    public int deleteOaMeetingById(Long id);
    
    /**
     * 审核会议申请
     * 
     * @param oaMeeting 会议申请
     * @return 结果
     */
    public int checkMeeting(OaMeeting oaMeeting);
}
