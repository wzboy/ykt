package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaAsset;

import java.util.List;

/**
 * 资产信息Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaAssetService 
{
    /**
     * 查询资产信息
     * 
     * @param id 资产信息ID
     * @return 资产信息
     */
    public OaAsset selectOaAssetById(Long id);

    /**
     * 查询资产信息列表
     * 
     * @param oaAsset 资产信息
     * @return 资产信息集合
     */
    public List<OaAsset> selectOaAssetList(OaAsset oaAsset);

    /**
     * 新增资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    public int insertOaAsset(OaAsset oaAsset);

    /**
     * 修改资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    public int updateOaAsset(OaAsset oaAsset);

    /**
     * 批量删除资产信息
     * 
     * @param ids 需要删除的资产信息ID
     * @return 结果
     */
    public int deleteOaAssetByIds(Long[] ids);

    /**
     * 删除资产信息信息
     * 
     * @param id 资产信息ID
     * @return 结果
     */
    public int deleteOaAssetById(Long id);
    

    /**
     * 导入资产数据
     * 
     * @param list List<OaAsset> 资产数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String imports(List<OaAsset> list, Boolean isUpdateSupport, String operName);
}
