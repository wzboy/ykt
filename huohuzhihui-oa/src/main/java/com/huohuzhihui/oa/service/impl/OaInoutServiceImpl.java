package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.oa.domain.OaInout;
import com.huohuzhihui.oa.mapper.OaInoutMapper;
import com.huohuzhihui.oa.service.IOaInoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 出入记录Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-10
 */
@Service
public class OaInoutServiceImpl implements IOaInoutService 
{
    @Autowired
    private OaInoutMapper oaInoutMapper;

    /**
     * 查询出入记录
     * 
     * @param id 出入记录ID
     * @return 出入记录
     */
    @Override
    public OaInout selectOaInoutById(Long id)
    {
        return oaInoutMapper.selectOaInoutById(id);
    }

    /**
     * 查询出入记录列表
     * 
     * @param oaInout 出入记录
     * @return 出入记录
     */
    @Override
    public List<OaInout> selectOaInoutList(OaInout oaInout)
    {
        return oaInoutMapper.selectOaInoutList(oaInout);
    }

    /**
     * 新增出入记录
     * 
     * @param oaInout 出入记录
     * @return 结果
     */
    @Override
    public int insertOaInout(OaInout oaInout)
    {
    	oaInout.setDay(DateUtils.dateTime(oaInout.getInTime()));
        return oaInoutMapper.insertOaInout(oaInout);
    }

    /**
     * 修改出入记录
     * 
     * @param oaInout 出入记录
     * @return 结果
     */
    @Override
    public int updateOaInout(OaInout oaInout)
    {
    	oaInout.setDay(DateUtils.dateTime(oaInout.getInTime()));
        return oaInoutMapper.updateOaInout(oaInout);
    }

    /**
     * 批量删除出入记录
     * 
     * @param ids 需要删除的出入记录ID
     * @return 结果
     */
    @Override
    public int deleteOaInoutByIds(Long[] ids)
    {
        return oaInoutMapper.deleteOaInoutByIds(ids);
    }

    /**
     * 删除出入记录信息
     * 
     * @param id 出入记录ID
     * @return 结果
     */
    @Override
    public int deleteOaInoutById(Long id)
    {
        return oaInoutMapper.deleteOaInoutById(id);
    }
}
