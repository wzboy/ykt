package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaReport;
import com.huohuzhihui.oa.mapper.OaReportMapper;
import com.huohuzhihui.oa.service.IOaReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作汇报Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaReportServiceImpl implements IOaReportService 
{
    @Autowired
    private OaReportMapper oaReportMapper;

    /**
     * 查询工作汇报
     * 
     * @param id 工作汇报ID
     * @return 工作汇报
     */
    @Override
    public OaReport selectOaReportById(Long id)
    {
    	OaReport oaReport = oaReportMapper.selectOaReportById(id);
    	if(oaReport.getReadTime() == null && oaReport.getToUser().equals(SecurityUtils.getLoginUser().getUser().getUserId())) {
    		oaReport.setReadTime(DateUtils.getNowDate());
    		oaReportMapper.updateOaReport(oaReport);
    	}
    	return oaReport;
    }

    /**
     * 查询工作汇报列表
     * 
     * @param oaReport 工作汇报
     * @return 工作汇报
     */
    @Override
    public List<OaReport> selectOaReportList(OaReport oaReport)
    {
        return oaReportMapper.selectOaReportList(oaReport);
    }

    /**
     * 新增工作汇报
     * 
     * @param oaReport 工作汇报
     * @return 结果
     */
    @Override
    public int insertOaReport(OaReport oaReport)
    {
    	oaReport.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
    	oaReport.setAddTime(DateUtils.getNowDate());
        return oaReportMapper.insertOaReport(oaReport);
    }

    /**
     * 修改工作汇报
     * 
     * @param oaReport 工作汇报
     * @return 结果
     */
    @Override
    public int updateOaReport(OaReport oaReport)
    {
        return oaReportMapper.updateOaReport(oaReport);
    }

    /**
     * 批量删除工作汇报
     * 
     * @param ids 需要删除的工作汇报ID
     * @return 结果
     */
    @Override
    public int deleteOaReportByIds(Long[] ids)
    {
        return oaReportMapper.deleteOaReportByIds(ids);
    }

    /**
     * 删除工作汇报信息
     * 
     * @param id 工作汇报ID
     * @return 结果
     */
    @Override
    public int deleteOaReportById(Long id)
    {
        return oaReportMapper.deleteOaReportById(id);
    }
}
