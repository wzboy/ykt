package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaDivan;

import java.util.List;

/**
 * 会议室Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaDivanMapper 
{
    /**
     * 查询会议室
     * 
     * @param id 会议室ID
     * @return 会议室
     */
    public OaDivan selectOaDivanById(Long id);

    /**
     * 查询会议室列表
     * 
     * @param oaDivan 会议室
     * @return 会议室集合
     */
    public List<OaDivan> selectOaDivanList(OaDivan oaDivan);

    /**
     * 新增会议室
     * 
     * @param oaDivan 会议室
     * @return 结果
     */
    public int insertOaDivan(OaDivan oaDivan);

    /**
     * 修改会议室
     * 
     * @param oaDivan 会议室
     * @return 结果
     */
    public int updateOaDivan(OaDivan oaDivan);

    /**
     * 删除会议室
     * 
     * @param id 会议室ID
     * @return 结果
     */
    public int deleteOaDivanById(Long id);

    /**
     * 批量删除会议室
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaDivanByIds(Long[] ids);
}
