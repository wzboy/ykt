package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaFlow;

import java.util.List;

/**
 * 工作流Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaFlowMapper 
{
    /**
     * 查询工作流
     * 
     * @param id 工作流ID
     * @return 工作流
     */
    public OaFlow selectOaFlowById(Long id);

    /**
     * 查询工作流列表
     * 
     * @param oaFlow 工作流
     * @return 工作流集合
     */
    public List<OaFlow> selectOaFlowList(OaFlow oaFlow);

    /**
     * 新增工作流
     * 
     * @param oaFlow 工作流
     * @return 结果
     */
    public int insertOaFlow(OaFlow oaFlow);

    /**
     * 修改工作流
     * 
     * @param oaFlow 工作流
     * @return 结果
     */
    public int updateOaFlow(OaFlow oaFlow);

    /**
     * 删除工作流
     * 
     * @param id 工作流ID
     * @return 结果
     */
    public int deleteOaFlowById(Long id);

    /**
     * 批量删除工作流
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaFlowByIds(Long[] ids);
}
