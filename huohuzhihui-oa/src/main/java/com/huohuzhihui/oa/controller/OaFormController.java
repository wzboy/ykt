package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaForm;
import com.huohuzhihui.oa.service.IOaFormService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公文单Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("公文单信息管理")
@RestController
@RequestMapping("/oa/form")
public class OaFormController extends BaseController
{
    @Autowired
    private IOaFormService oaFormService;

    /**
     * 查询公文单列表
     */
    @ApiOperation("获取公文单列表")
    @PreAuthorize("@ss.hasPermi('oa:form:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaForm oaForm)
    {
        startPage();
        List<OaForm> list = oaFormService.selectOaFormList(oaForm);
        return getDataTable(list);
    }

    /**
     * 导出公文单列表
     */
    @ApiOperation("导出公文单列表")
    @PreAuthorize("@ss.hasPermi('oa:form:export')")
    @Log(title = "公文单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaForm oaForm)
    {
        List<OaForm> list = oaFormService.selectOaFormList(oaForm);
        ExcelUtil<OaForm> util = new ExcelUtil<OaForm>(OaForm.class);
        return util.exportExcel(list, "form");
    }

    /**
     * 获取公文单详细信息
     */
    @ApiOperation("获取公文单详细信息")
    @ApiImplicitParam(name = "id", value = "公文单ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:form:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaFormService.selectOaFormById(id));
    }

    /**
     * 新增公文单
     */
    @ApiOperation("新增公文单")
    @PreAuthorize("@ss.hasPermi('oa:form:add')")
    @Log(title = "公文单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaForm oaForm)
    {
        return toAjax(oaFormService.insertOaForm(oaForm));
    }

    /**
     * 修改公文单
     */
    @ApiOperation("修改公文单")
    @PreAuthorize("@ss.hasPermi('oa:form:edit')")
    @Log(title = "公文单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaForm oaForm)
    {
        return toAjax(oaFormService.updateOaForm(oaForm));
    }

    /**
     * 删除公文单
     */
    @ApiOperation("删除公文单")
    @ApiImplicitParam(name = "id", value = "公文单ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:form:remove')")
    @Log(title = "公文单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaFormService.deleteOaFormByIds(ids));
    }
}
