package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaMeetingSummary;
import com.huohuzhihui.oa.service.IOaMeetingSummaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会议记要Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("会议记要信息管理")
@RestController
@RequestMapping("/oa/summary")
public class OaMeetingSummaryController extends BaseController
{
    @Autowired
    private IOaMeetingSummaryService oaMeetingSummaryService;

    /**
     * 查询会议记要列表
     */
    @ApiOperation("获取会议记要列表")
    @PreAuthorize("@ss.hasPermi('oa:summary:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaMeetingSummary oaMeetingSummary)
    {
        startPage();
        List<OaMeetingSummary> list = oaMeetingSummaryService.selectOaMeetingSummaryList(oaMeetingSummary);
        return getDataTable(list);
    }

    /**
     * 导出会议记要列表
     */
    @ApiOperation("导出会议记要列表")
    @PreAuthorize("@ss.hasPermi('oa:summary:export')")
    @Log(title = "会议记要", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaMeetingSummary oaMeetingSummary)
    {
        List<OaMeetingSummary> list = oaMeetingSummaryService.selectOaMeetingSummaryList(oaMeetingSummary);
        ExcelUtil<OaMeetingSummary> util = new ExcelUtil<OaMeetingSummary>(OaMeetingSummary.class);
        return util.exportExcel(list, "summary");
    }

    /**
     * 获取会议记要详细信息
     */
    @ApiOperation("获取会议记要详细信息")
    @ApiImplicitParam(name = "id", value = "会议记要ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:summary:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaMeetingSummaryService.selectOaMeetingSummaryById(id));
    }

    /**
     * 新增会议记要
     */
    @ApiOperation("新增会议记要")
    @PreAuthorize("@ss.hasPermi('oa:summary:add')")
    @Log(title = "会议记要", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaMeetingSummary oaMeetingSummary)
    {
        return toAjax(oaMeetingSummaryService.insertOaMeetingSummary(oaMeetingSummary));
    }

    /**
     * 修改会议记要
     */
    @ApiOperation("修改会议记要")
    @PreAuthorize("@ss.hasPermi('oa:summary:edit')")
    @Log(title = "会议记要", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaMeetingSummary oaMeetingSummary)
    {
        return toAjax(oaMeetingSummaryService.updateOaMeetingSummary(oaMeetingSummary));
    }

    /**
     * 删除会议记要
     */
    @ApiOperation("删除会议记要")
    @ApiImplicitParam(name = "id", value = "会议记要ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:summary:remove')")
    @Log(title = "会议记要", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaMeetingSummaryService.deleteOaMeetingSummaryByIds(ids));
    }
}
