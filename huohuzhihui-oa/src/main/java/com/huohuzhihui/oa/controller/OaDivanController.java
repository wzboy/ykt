package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaDivan;
import com.huohuzhihui.oa.service.IOaDivanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会议室Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("会议室信息管理")
@RestController
@RequestMapping("/oa/divan")
public class OaDivanController extends BaseController
{
    @Autowired
    private IOaDivanService oaDivanService;

    /**
     * 查询会议室列表
     */
    @ApiOperation("获取会议室列表")
    @PreAuthorize("@ss.hasPermi('oa:divan:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaDivan oaDivan)
    {
        startPage();
        List<OaDivan> list = oaDivanService.selectOaDivanList(oaDivan);
        return getDataTable(list);
    }
    

    /**
     * 查询会议室列表
     */
    @ApiOperation("获取会议室列表")
    @GetMapping("/select")
    public TableDataInfo select(OaDivan oaDivan)
    {
        startPage();
        List<OaDivan> list = oaDivanService.selectOaDivanList(oaDivan);
        return getDataTable(list);
    }

    /**
     * 导出会议室列表
     */
    @ApiOperation("导出会议室列表")
    @PreAuthorize("@ss.hasPermi('oa:divan:export')")
    @Log(title = "会议室", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaDivan oaDivan)
    {
        List<OaDivan> list = oaDivanService.selectOaDivanList(oaDivan);
        ExcelUtil<OaDivan> util = new ExcelUtil<OaDivan>(OaDivan.class);
        return util.exportExcel(list, "会议室");
    }

    /**
     * 获取会议室详细信息
     */
    @ApiOperation("获取会议室详细信息")
    @ApiImplicitParam(name = "id", value = "会议室ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:divan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaDivanService.selectOaDivanById(id));
    }

    /**
     * 新增会议室
     */
    @ApiOperation("新增会议室")
    @PreAuthorize("@ss.hasPermi('oa:divan:add')")
    @Log(title = "会议室", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaDivan oaDivan)
    {
        return toAjax(oaDivanService.insertOaDivan(oaDivan));
    }

    /**
     * 修改会议室
     */
    @ApiOperation("修改会议室")
    @PreAuthorize("@ss.hasPermi('oa:divan:edit')")
    @Log(title = "会议室", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaDivan oaDivan)
    {
        return toAjax(oaDivanService.updateOaDivan(oaDivan));
    }

    /**
     * 删除会议室
     */
    @ApiOperation("删除会议室")
    @ApiImplicitParam(name = "id", value = "会议室ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:divan:remove')")
    @Log(title = "会议室", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaDivanService.deleteOaDivanByIds(ids));
    }
}
