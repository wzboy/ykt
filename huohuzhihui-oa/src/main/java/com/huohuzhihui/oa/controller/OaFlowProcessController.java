package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaFlowProcess;
import com.huohuzhihui.oa.service.IOaFlowProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作流步骤Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("工作流步骤信息管理")
@RestController
@RequestMapping("/oa/process")
public class OaFlowProcessController extends BaseController
{
    @Autowired
    private IOaFlowProcessService oaFlowProcessService;

    /**
     * 查询工作流步骤列表
     */
    @ApiOperation("获取工作流步骤列表")
    @PreAuthorize("@ss.hasPermi('oa:process:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaFlowProcess oaFlowProcess)
    {
        startPage();
        List<OaFlowProcess> list = oaFlowProcessService.selectOaFlowProcessList(oaFlowProcess);
        return getDataTable(list);
    }

    /**
     * 查询个人工作流步骤列表
     */
    @ApiOperation("获取个工作流步骤列表")
    @PreAuthorize("@ss.hasPermi('oa:process:my')")
    @GetMapping("/my")
    public TableDataInfo my(OaFlowProcess oaFlowProcess)
    {
        startPage();
        oaFlowProcess.setUserId(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaFlowProcess> list = oaFlowProcessService.selectOaFlowProcessList(oaFlowProcess);
        return getDataTable(list);
    }
    
    /**
     * 导出工作流步骤列表
     */
    @ApiOperation("导出工作流步骤列表")
    @PreAuthorize("@ss.hasPermi('oa:process:export')")
    @Log(title = "工作流步骤", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaFlowProcess oaFlowProcess)
    {
        List<OaFlowProcess> list = oaFlowProcessService.selectOaFlowProcessList(oaFlowProcess);
        ExcelUtil<OaFlowProcess> util = new ExcelUtil<OaFlowProcess>(OaFlowProcess.class);
        return util.exportExcel(list, "process");
    }

    /**
     * 获取工作流步骤详细信息
     */
    @ApiOperation("获取工作流步骤详细信息")
    @ApiImplicitParam(name = "id", value = "工作流步骤ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaFlowProcessService.selectOaFlowProcessById(id));
    }

    /**
     * 新增工作流步骤
     */
    @ApiOperation("新增工作流步骤")
    @PreAuthorize("@ss.hasPermi('oa:process:add')")
    @Log(title = "工作流步骤", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaFlowProcess oaFlowProcess)
    {
        return toAjax(oaFlowProcessService.insertOaFlowProcess(oaFlowProcess));
    }

    /**
     * 修改工作流步骤
     */
    @ApiOperation("修改工作流步骤")
    @PreAuthorize("@ss.hasPermi('oa:process:edit')")
    @Log(title = "工作流步骤", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaFlowProcess oaFlowProcess)
    {
        return toAjax(oaFlowProcessService.updateOaFlowProcess(oaFlowProcess));
    }

    /**
     * 删除工作流步骤
     */
    @ApiOperation("删除工作流步骤")
    @ApiImplicitParam(name = "id", value = "工作流步骤ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:process:remove')")
    @Log(title = "工作流步骤", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaFlowProcessService.deleteOaFlowProcessByIds(ids));
    }
    
    /**
     * 签收工作流
     */
    @ApiOperation("签收工作流")
    @ApiImplicitParam(name = "id", value = "工作流步骤ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:process:edit')")
    @Log(title = "工作流步骤", businessType = BusinessType.UPDATE)
	@GetMapping("/signin/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(oaFlowProcessService.signinOaFlowProcess(id));
    }
}
