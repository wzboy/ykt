package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaCategory;
import com.huohuzhihui.oa.service.IOaCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 资产分类Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("资产分类信息管理")
@RestController
@RequestMapping("/oa/category")
public class OaCategoryController extends BaseController
{
    @Autowired
    private IOaCategoryService oaCategoryService;

    /**
     * 查询资产分类列表
     */
    @ApiOperation("获取资产分类列表")
    @PreAuthorize("@ss.hasPermi('oa:category:list')")
    @GetMapping("/list")
    public AjaxResult list(OaCategory oaCategory)
    {
        List<OaCategory> list = oaCategoryService.selectOaCategoryList(oaCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出资产分类列表
     */
    @ApiOperation("导出资产分类列表")
    @PreAuthorize("@ss.hasPermi('oa:category:export')")
    @Log(title = "资产分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaCategory oaCategory)
    {
        List<OaCategory> list = oaCategoryService.selectOaCategoryList(oaCategory);
        ExcelUtil<OaCategory> util = new ExcelUtil<OaCategory>(OaCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取资产分类详细信息
     */
    @ApiOperation("获取资产分类详细信息")
    @ApiImplicitParam(name = "id", value = "资产分类ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaCategoryService.selectOaCategoryById(id));
    }

    /**
     * 新增资产分类
     */
    @ApiOperation("新增资产分类")
    @PreAuthorize("@ss.hasPermi('oa:category:add')")
    @Log(title = "资产分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaCategory oaCategory)
    {
        return toAjax(oaCategoryService.insertOaCategory(oaCategory));
    }

    /**
     * 修改资产分类
     */
    @ApiOperation("修改资产分类")
    @PreAuthorize("@ss.hasPermi('oa:category:edit')")
    @Log(title = "资产分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaCategory oaCategory)
    {
        return toAjax(oaCategoryService.updateOaCategory(oaCategory));
    }

    /**
     * 删除资产分类
     */
    @ApiOperation("删除资产分类")
    @ApiImplicitParam(name = "id", value = "资产分类ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:category:remove')")
    @Log(title = "资产分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaCategoryService.deleteOaCategoryByIds(ids));
    }
}
