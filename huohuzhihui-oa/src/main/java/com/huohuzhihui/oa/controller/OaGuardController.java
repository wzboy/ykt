package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaGuard;
import com.huohuzhihui.oa.service.IOaGuardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门禁Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("门禁信息管理")
@RestController
@RequestMapping("/oa/guard")
public class OaGuardController extends BaseController
{
    @Autowired
    private IOaGuardService oaGuardService;

    /**
     * 查询门禁列表
     */
    @ApiOperation("获取门禁列表")
    @PreAuthorize("@ss.hasPermi('oa:guard:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaGuard oaGuard)
    {
        startPage();
        List<OaGuard> list = oaGuardService.selectOaGuardList(oaGuard);
        return getDataTable(list);
    }

    /**
     * 导出门禁列表
     */
    @ApiOperation("导出门禁列表")
    @PreAuthorize("@ss.hasPermi('oa:guard:export')")
    @Log(title = "门禁", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaGuard oaGuard)
    {
        List<OaGuard> list = oaGuardService.selectOaGuardList(oaGuard);
        ExcelUtil<OaGuard> util = new ExcelUtil<OaGuard>(OaGuard.class);
        return util.exportExcel(list, "guard");
    }

    /**
     * 获取门禁详细信息
     */
    @ApiOperation("获取门禁详细信息")
    @ApiImplicitParam(name = "id", value = "门禁ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:guard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaGuardService.selectOaGuardById(id));
    }

    /**
     * 新增门禁
     */
    @ApiOperation("新增门禁")
    @PreAuthorize("@ss.hasPermi('oa:guard:add')")
    @Log(title = "门禁", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaGuard oaGuard)
    {
        return toAjax(oaGuardService.insertOaGuard(oaGuard));
    }

    /**
     * 修改门禁
     */
    @ApiOperation("修改门禁")
    @PreAuthorize("@ss.hasPermi('oa:guard:edit')")
    @Log(title = "门禁", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaGuard oaGuard)
    {
        return toAjax(oaGuardService.updateOaGuard(oaGuard));
    }

    /**
     * 删除门禁
     */
    @ApiOperation("删除门禁")
    @ApiImplicitParam(name = "id", value = "门禁ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:guard:remove')")
    @Log(title = "门禁", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaGuardService.deleteOaGuardByIds(ids));
    }
}
