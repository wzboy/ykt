package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.system.domain.SysFiles;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 会议记要对象 oa_meeting_summary
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("会议记要实体")
@Getter
@Setter
public class OaMeetingSummary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 会议 */
    @Excel(name = "会议")
    @ApiModelProperty("会议")
    private Long meetId;
    private String meetName;

    /** 内容 */
    @Excel(name = "内容")
    @ApiModelProperty("内容")
    private String content;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty("附件")
    private Long fileId;
    private SysFiles file;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("meetId", getMeetId())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("content", getContent())
            .append("fileId", getFileId())
            .toString();
    }
}
