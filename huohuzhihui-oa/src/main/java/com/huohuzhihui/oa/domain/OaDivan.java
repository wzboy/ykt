package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 会议室对象 oa_divan
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("会议室实体")
@Getter
@Setter
public class OaDivan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 会议室名称 */
    @Excel(name = "会议室名称")
    @ApiModelProperty("会议室名称")
    private String title;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty("联系人")
    private String linkman;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty("联系电话")
    private String phone;

    /** 地址 */
    @Excel(name = "地址")
    @ApiModelProperty("地址")
    private String address;

    /** 最大容量 */
    @Excel(name = "最大容量")
    @ApiModelProperty("最大容量")
    private Long maxs;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty("排序")
    private Long listSort;

    /** 状态 */
    @Excel(name = "状态", dictType = "common_status")
    @ApiModelProperty("状态")
    private String status;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("linkman", getLinkman())
            .append("phone", getPhone())
            .append("address", getAddress())
            .append("maxs", getMaxs())
            .append("listSort", getListSort())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
