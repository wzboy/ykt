import request from '@/utils/request'

// 查询公文单列表
export function listForm(query) {
  return request({
    url: '/oa/form/list',
    method: 'get',
    params: query
  })
}

// 查询公文单详细
export function getForm(id) {
  return request({
    url: '/oa/form/' + id,
    method: 'get'
  })
}

// 新增公文单
export function addForm(data) {
  return request({
    url: '/oa/form',
    method: 'post',
    data: data
  })
}

// 修改公文单
export function updateForm(data) {
  return request({
    url: '/oa/form',
    method: 'put',
    data: data
  })
}

// 删除公文单
export function delForm(id) {
  return request({
    url: '/oa/form/' + id,
    method: 'delete'
  })
}

// 导出公文单
export function exportForm(query) {
  return request({
    url: '/oa/form/export',
    method: 'get',
    params: query
  })
}