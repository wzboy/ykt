import request from '@/utils/request'

// 查询资产信息列表
export function listAsset(query) {
  return request({
    url: '/oa/asset/list',
    method: 'get',
    params: query
  })
}

// 查询资产信息详细
export function getAsset(id) {
  return request({
    url: '/oa/asset/' + id,
    method: 'get'
  })
}

// 新增资产信息
export function addAsset(data) {
  return request({
    url: '/oa/asset',
    method: 'post',
    data: data
  })
}

// 修改资产信息
export function updateAsset(data) {
  return request({
    url: '/oa/asset',
    method: 'put',
    data: data
  })
}

// 删除资产信息
export function delAsset(id) {
  return request({
    url: '/oa/asset/' + id,
    method: 'delete'
  })
}

// 导出资产信息
export function exportAsset(query) {
  return request({
    url: '/oa/asset/export',
    method: 'get',
    params: query
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/oa/asset/importTemplate',
    method: 'get'
  })
}
