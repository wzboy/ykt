import request from '@/utils/request'

// 查询楼宇列表
export function listBuilding(query) {
  return request({
    url: '/base/building/list',
    method: 'get',
    params: query
  })
}

// 查询楼宇下拉列表
export function selectBuilding(query) {
  return request({
    url: '/base/building/select',
    method: 'get',
    params: query
  })
}

// 查询楼宇详细
export function getBuilding(id) {
  return request({
    url: '/base/building/' + id,
    method: 'get'
  })
}

// 新增楼宇
export function addBuilding(data) {
  return request({
    url: '/base/building',
    method: 'post',
    data: data
  })
}

// 修改楼宇
export function updateBuilding(data) {
  return request({
    url: '/base/building',
    method: 'put',
    data: data
  })
}

// 删除楼宇
export function delBuilding(id) {
  return request({
    url: '/base/building/' + id,
    method: 'delete'
  })
}

// 导出楼宇
export function exportBuilding(query) {
  return request({
    url: '/base/building/export',
    method: 'get',
    params: query
  })
}