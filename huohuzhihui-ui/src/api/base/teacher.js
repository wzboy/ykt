import request from '@/utils/request'

// 查询教师列表
export function listTeacher(query) {
  return request({
    url: '/base/teacher/list',
    method: 'get',
    params: query
  })
}

// 查询教师列表
export function selectTeacher(query) {
  return request({
    url: '/base/teacher/select',
    method: 'get',
    params: query
  })
}

// 查询教师详细
export function getTeacher(id) {
  return request({
    url: '/base/teacher/' + id,
    method: 'get'
  })
}

// 新增教师
export function addTeacher(data) {
  return request({
    url: '/base/teacher',
    method: 'post',
    data: data
  })
}

// 修改教师
export function updateTeacher(data) {
  return request({
    url: '/base/teacher',
    method: 'put',
    data: data
  })
}

// 删除教师
export function delTeacher(id) {
  return request({
    url: '/base/teacher/' + id,
    method: 'delete'
  })
}

// 导出教师
export function exportTeacher(query) {
  return request({
    url: '/base/teacher/export',
    method: 'get',
    params: query
  })
}

// 启用教师
export function enableTeacher(id) {
  return request({
    url: '/base/teacher/enable/' + id,
    method: 'get'
  })
}


// 禁用教师
export function disableTeacher(id) {
  return request({
    url: '/base/teacher/disable/' + id,
    method: 'get'
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/teacher/importTemplate',
    method: 'get'
  })
}